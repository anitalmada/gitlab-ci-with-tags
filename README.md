## Files and pipeline for CI/CD using GitLab
This project was designed to provide an example followed on a blog post tutorial. It brings the basics files to create Docker image of a NodeJS app and a gitlab-ci pipeline template. This pipeline executes two main stages: 
* build, tag and push a Docker image when a git tag is created;
* and a custom deployment of that image depending on the branch where the commit was made and wether the commit is a tag or not.
